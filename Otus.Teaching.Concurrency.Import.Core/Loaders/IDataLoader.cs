﻿namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public interface IDataLoader<T>
    {
        void LoadData(T[] data);
    }
}