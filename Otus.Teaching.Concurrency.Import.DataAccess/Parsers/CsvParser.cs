﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.DataGenerator.Serializers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        public List<Customer> Parse(string fileName)
        {
            string input = File.ReadAllText(fileName);
            CustomersList list = CustomersListCsvSerializer.Deserialize(input);
            return list.Customers;
        }
    }
}
