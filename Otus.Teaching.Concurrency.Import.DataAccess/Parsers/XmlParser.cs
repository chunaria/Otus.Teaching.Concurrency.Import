﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        public List<Customer> Parse(string fileName)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(CustomersList));
            CustomersList obj;
            using(FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                obj = (CustomersList)serializer.Deserialize(fs);
            }
            return obj?.Customers;
        }
    }
}