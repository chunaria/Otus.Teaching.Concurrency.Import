using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository, IDisposable
    {
        private CustomersContext context;

        private static int tryCount = 3;

        public CustomerRepository()
        {
            context = new CustomersContext();
        }

        public void DeleteAll()
        {
            context.RemoveRange(context.Customers);
            context.SaveChanges();
        }

        public IEnumerable<Customer> GetCustomers()
        {
            return context.Customers;
        }
        public void AddCustomer(Customer customer)
        {
            int counter = 1;
            while (counter <= tryCount)
            {
                try
                {
                    context.Customers.Add(customer);
                    context.SaveChanges();
                    return;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"������� {counter} �� ������� : {e.Message}");
                }
            }
        }

        public void AddCustomers(Customer[] customers)
        {
            int counter = 0;
            while (counter < tryCount)
            {
                try
                {
                    context.Customers.AddRange(customers);
                    context.SaveChanges();
                    return;
                }
                catch (Exception e)
                {
                    counter++;
                    Console.WriteLine($"������� {counter} �� ������� : {e.Message}");
                }
            }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}