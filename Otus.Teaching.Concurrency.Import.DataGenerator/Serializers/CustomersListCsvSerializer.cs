﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Serializers
{
    public static class CustomersListCsvSerializer
    {
        private const string separator = ",";

        public static string Serialize(CustomersList customersList)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Customer customer in customersList.Customers)
            {
                sb.AppendLine($"{customer.Id},{customer.FullName},{customer.Email},{customer.Phone}");
            }
            return sb.ToString();
        }

        public static CustomersList Deserialize(string input)
        {
            List<Customer> customers = new List<Customer>();
            string[] allCustomerStrings = input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
            foreach (string oneCustomerString in allCustomerStrings)
            {
                string[] properties = oneCustomerString.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                if (properties.Length!=4)
                {
                    throw new ArgumentException($"Тип customer должен содержать 4 свойства, а в строке \"{oneCustomerString}\"записано {properties.Length}");
                }
                int id;
                if (!int.TryParse(properties[0], out id))
                {
                    throw new ArgumentException($"Описание типа customer должено начинаться с id - целого числа, а начинается с {properties[0]}");
                }
                customers.Add(new Customer{Id = id, FullName = properties[1], Email = properties[2], Phone = properties[3]});
            }
            return new CustomersList{ Customers = customers};
        }

    }
}
