﻿using System;
using System.IO;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.DataGenerator.Serializers;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CsvGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            try
            {
                File.WriteAllText(_fileName, CustomersListCsvSerializer.Serialize(new CustomersList{Customers = customers }));
            }
            catch(Exception e)
            {
                Console.WriteLine($"Ошибка при генерации файла {_fileName}: {e.Message}");
            }
        }
    }
}
