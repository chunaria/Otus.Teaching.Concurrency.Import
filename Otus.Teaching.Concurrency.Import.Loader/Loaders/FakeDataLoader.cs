using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class DataLoader
        : IDataLoader<Customer>
    {
        public void LoadData(Customer[] data)
        {
       //     Console.WriteLine($"Loading data in thread{Thread.CurrentThread.ManagedThreadId}...");
            using(CustomerRepository repo = new CustomerRepository())
            {
                repo.AddCustomers(data);
            }
     //       Console.WriteLine($"Loaded data in thread{Thread.CurrentThread.ManagedThreadId}...");
        }
    }
}