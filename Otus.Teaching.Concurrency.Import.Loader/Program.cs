﻿using System;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers");

        static void Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }
            UI ui = new UI(new DataManager(), _dataFilePath);
            ui.MainCycle();
        }
    }
}