﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class UI
    {
        private static string _dataFilePath;
        private DataManager _dataManager;

        public UI(DataManager dataManager, string dataFilePath)
        {
            _dataManager = dataManager;
            _dataFilePath = dataFilePath;
        }

        public static void PrintMenu()
        {
            Console.WriteLine("Выберите действие");
            Console.WriteLine("1 - Задать формат генерации данных, xml (по умолчанию) или csv");
            Console.WriteLine("2 - Задать способ генерации данных, в текущем процессе (по умолчанию) или в отдельном");
            Console.WriteLine("3 - Задать количество количество генерируемых пользователей, по умолчанию 1000");
            Console.WriteLine("4 - Задать количество потоков загрузки данных, по умолчанию 3");
            Console.WriteLine("5 - Запуск задачи с текущими параметрами и созданием указанного числа потоков");
            Console.WriteLine("6 - Запуск задачи с текущими параметрами через пул потоков");
            Console.WriteLine("7 - Выход");
        }

        private int GetInput(int limit, string errorPhrase)
        {
            string input = Console.ReadLine();
            int result;
            while (!int.TryParse(input, out result) || result <= 0 || result > limit)
            {
                Console.WriteLine(errorPhrase);
                input = Console.ReadLine();
            }

            return result;
        }

        private void SetDataMode()
        {
            Console.WriteLine("Задайте формат генерации данных: 1 - xml, 2 - csv");
            int modeNumber = GetInput(2, "Введите 1 для формата xml или 2 для формата csv");
            _dataManager.SwitchFileTypeMode(modeNumber == 1);
        }

        private void SetProcessMode()
        {
            Console.WriteLine("Задайте способ генерации данных: 1 - в текущем процессе, 2 - в отдельном процессе");
            int modeNumber = GetInput(2, "Введите 1 для генерации в текущем процессе или 2 для генерации в отдельном процессе");
            _dataManager.SwitchProcessMode(modeNumber == 1);
        }

        private void SetCustomersCount()
        {
            Console.WriteLine("Задайте число пользователей для генерации");
            int number = GetInput(int.MaxValue, "Введите целое положительное число");
            _dataManager.SetCustomersCount(number);
        }

        private void SetThreadsCount()
        {
            Console.WriteLine("Задайте число потоков для обработки");
            int number = GetInput(int.MaxValue, "Введите целое положительное число");
            _dataManager.SetThreadsCount(number);
        }

        public void MainCycle()
        {
            while (true)
            {
                PrintMenu();
                int command = GetInput(7, "Введите корректный номер команды");
                switch (command)
                {
                    case 1:
                        SetDataMode();
                        Console.WriteLine($"Формат данных установлен");
                        Console.ReadLine();
                        break;
                    case 2:
                        SetProcessMode();
                        Console.WriteLine($"Режим генерации установлен");
                        Console.ReadLine();
                        break;
                    case 3:
                        SetCustomersCount();
                        Console.WriteLine($"Количество гернерируемых пользователей установлено");
                        Console.ReadLine();
                        break;
                    case 4:
                        SetThreadsCount();
                        Console.WriteLine($"Количество потоков для обработки установлено");
                        Console.ReadLine();
                        break;
                    case 5:
                        TimeSpan tThreads = TreatmentByThreads();
                        Console.WriteLine($"Обработка отдельными потоками завершена за {tThreads}");
                        Console.ReadLine();
                        break;
                    case 6:
                        TimeSpan tPool = TreatmentByThreadPool();
                        Console.WriteLine($"Обработка пулом потоков завершена за {tPool}");
                        Console.ReadLine();
                        break;
                    case 7:
                        return;
                }
            }
        }

        private TimeSpan TreatmentByThreads()
        {
            _dataManager.CleanUpOldData();
            _dataManager.GenerateData(_dataFilePath);
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            _dataManager.LoadDataByThreads(_dataFilePath);
            stopWatch.Stop();
            return stopWatch.Elapsed;
        }

        private TimeSpan TreatmentByThreadPool()
        {
            _dataManager.CleanUpOldData();
            _dataManager.GenerateData(_dataFilePath);
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            _dataManager.LoadDataByThreadPool(_dataFilePath);
            stopWatch.Stop();
            return stopWatch.Elapsed;
        }
    }
}
