﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class DataManager
    {
        private string _generatorFilePath = Path.Combine(@"C:\OTUS\ConcurrencyImport\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Debug\netcoreapp3.1", @"Otus.Teaching.Concurrency.Import.DataGenerator.App.exe");
        private static bool _xmlMode = true;
        private static bool _oneProcessMode = true;
        private int _count = 1000;
        private int _threadCount = 3;

        public void SwitchFileTypeMode(bool xmlMode)
        {
            _xmlMode = xmlMode;
        }

        public void SwitchProcessMode(bool sameProcess)
        {
            _oneProcessMode = sameProcess;
        }

        public void SetCustomersCount(int count)
        {
            _count = count;
        }

        public void SetThreadsCount(int count)
        {
            _threadCount = count;
        }

        private void GenerateCustomersDataFile(string dataFilePath)
        {
            IDataGenerator generator;
            if (_xmlMode)
                generator = new XmlGenerator(dataFilePath + ".xml", _count);
            else
                generator = new CsvGenerator(dataFilePath, _count);
            generator.Generate();
        }

        private void GenerateCustomersDataFileInOtherProcess(string dataFilePath)
        {
            var startInfo = new ProcessStartInfo()
            {
                ArgumentList = { _xmlMode ? dataFilePath + ".xml" : dataFilePath, _count.ToString(), _xmlMode ? string.Empty : "csv" },
                FileName = _generatorFilePath
            };

            using (var process = Process.Start(startInfo))
            {
                process.WaitForExit();
            }
        }

        public void GenerateData(string dataFilePath)
        {
            if (_oneProcessMode)
                GenerateCustomersDataFile(dataFilePath);
            else
                GenerateCustomersDataFileInOtherProcess(dataFilePath);
        }

        public void CleanUpOldData()
        {
            using (CustomerRepository repo = new CustomerRepository())
            {
                repo.DeleteAll();
            }
        }

        private void LoadData(object data)
        {
            Customer[] customers = data as Customer[];
            DataLoader loader = new DataLoader();
            loader.LoadData(customers);
        }

        public void LoadDataByThreads(string dataFilePath)
        {
            List<Customer> customers = ParseData(dataFilePath);
            int size, rem;
            size = Math.DivRem(customers.Count, _threadCount, out rem);
            Customer[][] customerBlocks = new Customer[_threadCount][];
            Thread[] threads = new Thread[_threadCount];

            for (int i = 0; i < _threadCount; i++)
            {
                int blockSize = i == _threadCount - 1 ? size + rem : size;
                customerBlocks[i] = new Customer[blockSize];
                customers.CopyTo(i * size, customerBlocks[i], 0, blockSize);
                threads[i] = new Thread(new ParameterizedThreadStart(LoadData));
                threads[i].Start(customerBlocks[i]);
            }

            foreach (Thread t in threads)
                t.Join();
        }

        public void LoadDataByThreadPool(string dataFilePath)
        {
            List<Customer> customers = ParseData(dataFilePath);
            int size, rem;
            size = Math.DivRem(customers.Count, _threadCount, out rem);
            Customer[][] customerBlocks = new Customer[_threadCount][];
            CountdownEvent cnt = new CountdownEvent(_threadCount);
            for (int i = 0; i < _threadCount; i++)
            {
                int blockSize = i == _threadCount - 1 ? size + rem : size;
                customerBlocks[i] = new Customer[blockSize];
                customers.CopyTo(i * size, customerBlocks[i], 0, blockSize);
                Tuple<Customer[], CountdownEvent> obj = new Tuple<Customer[], CountdownEvent>(customerBlocks[i], cnt);
                ThreadPool.QueueUserWorkItem(LoadDataInPool, obj);
            }
            cnt.Wait();
        }

        private void LoadDataInPool(object o)
        {
            Tuple<Customer[], CountdownEvent> p = o as Tuple<Customer[], CountdownEvent>;
            if (p == null)
            {
                return;
            }
            LoadData(p.Item1);
            p.Item2.Signal();
        }

        private List<Customer> ParseData(string dataFilePath)
        {
            string fileName;
            IDataParser<List<Customer>> parser;
            if (_xmlMode)
            {
                parser = new XmlParser();
                fileName = dataFilePath + ".xml";
            }
            else
            {
                parser = new CsvParser();
                fileName = dataFilePath;
            }
            return parser.Parse(fileName);
        }
    }
}
