using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace Otus.Teaching.Concurrency.Import.Generators
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(string fileName, int dataCount, bool xml = true)
        {
             IDataGenerator generator;
            if(xml)
                generator = new XmlGenerator(fileName, dataCount);
            else
                generator = new CsvGenerator(fileName, dataCount);
            return generator;
        }
    }
}