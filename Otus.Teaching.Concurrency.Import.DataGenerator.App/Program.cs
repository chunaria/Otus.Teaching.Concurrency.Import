﻿using System;
using System.IO;
using Otus.Teaching.Concurrency.Import.Generators;

namespace Otus.Teaching.Concurrency.Import.Generator
{
    class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName; 
        private static int _dataCount = 100; 
        private static bool xmlFlag = true;
        
        static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;
            
            Console.WriteLine("Generating data...");

            var generator = GeneratorFactory.GetGenerator(_dataFileName, _dataCount, xmlFlag);
            
            generator.Generate();
            
            Console.WriteLine($"Generated data in {_dataFileName}...");

        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                _dataFileName = Path.Combine(_dataFileDirectory, $"{args[0]}");
            }
            else
            {
                Console.WriteLine("Data file name without extension is required");
                return false;
            }
            
            if (args.Length > 1)
            {
                if (!int.TryParse(args[1], out _dataCount))
                {
                    Console.WriteLine("Data must be integer");
                    return false;
                }
            }

            if (args.Length > 2)
            {
                if ("csv".Equals(args[2],StringComparison.CurrentCultureIgnoreCase))
                {
                    xmlFlag = false;
                }
            }

            return true;
        }
    }
}